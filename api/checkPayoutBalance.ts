import { FlexBaseRequestPayload, FlexBaseResponsePayload } from "../types";
import { request } from "../utils";

type FlexPayoutBalance = {
  payout_balance: number;
};

type FlexCheckPayoutBalanceResponsePayload = {
  data: FlexPayoutBalance;
} & FlexBaseResponsePayload;

type CheckPayoutBalanceResponse = {
  payoutBalance: number;
};

const ENDPOINT = "partner/v1/balances";

const checkPayoutBalance = async (): Promise<CheckPayoutBalanceResponse> => {
  const payload: FlexBaseRequestPayload = {
    request_time: Date.now(),
  };

  const response = await request<FlexCheckPayoutBalanceResponsePayload>(ENDPOINT, payload);
  return {
    payoutBalance: response.data.payout_balance,
  };
};

export default checkPayoutBalance;
