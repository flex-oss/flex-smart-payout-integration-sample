import isEmpty from "lodash/isEmpty";
import { FlexBaseRequestPayload, FLEX_DISBURSEMENT_STATUSES } from "../types";
import { request } from "../utils";

type FlexPayee = {
  bank_id: string;
  bank_name: string;
  bank_account_number: string;
  bank_account_name: string;
};

export type FlexDisbursementInput = {
  ref_code: string;
  amount: number;
  note: string;
  payee: FlexPayee;
  labels?: string[];
};

type FlexCreateDisbursementRequestPayload = {
  data: FlexDisbursementInput[];
} & FlexBaseRequestPayload;

type FlexDisbursement = {
  id: string;
  ref_code: string;
  status: FLEX_DISBURSEMENT_STATUSES;
  created_at: string;
  processing_fee: number;
  amount: number;
  note: string;
  payee: FlexPayee;
  labels?: string[];
};

type FlexCreateDisbursementResponsePayload = {
  data: FlexDisbursement[];
};

const ENDPOINT = "partner/v1/disbursement";

const createDisbursement = async (disbursement: FlexDisbursementInput): Promise<FlexDisbursement> => {
  const payload: FlexCreateDisbursementRequestPayload = {
    request_time: Date.now(),
    data: [disbursement],
  };

  const response = await request<FlexCreateDisbursementResponsePayload>(ENDPOINT, payload);

  if (isEmpty(response.data) || response.data[0] === undefined) {
    console.info(JSON.stringify({ ENDPOINT, payload, response }, null, 2));
    throw new Error("FLEX ERROR: Disbursement creation failed", { cause: response });
  }

  return response.data[0];
};

export default createDisbursement;
