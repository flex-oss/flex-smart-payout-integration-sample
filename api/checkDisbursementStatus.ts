import { FlexBaseRequestPayload, FlexBaseResponsePayload, FLEX_DISBURSEMENT_STATUSES } from "../types";
import { request } from "../utils";

type FlexCheckDisbursementStatusRequestPayload = {
  ids: string[];
} & FlexBaseRequestPayload;
type FlexDisbursementStatus = {
  id: string;
  ref_code: string;
  status: FLEX_DISBURSEMENT_STATUSES;
};

type FlexCheckDisbursementStatusResponsePayload = {
  data: FlexDisbursementStatus[];
} & FlexBaseResponsePayload;

type CheckDisbursementStatusResponse = {
  refCode: string;
  status: FLEX_DISBURSEMENT_STATUSES;
};

const ENDPOINT = "partner/v1/disbursement/status";

const checkDisbursementStatus = async (ids: string[]): Promise<CheckDisbursementStatusResponse[]> => {
  const payload: FlexCheckDisbursementStatusRequestPayload = {
    ids,
    request_time: Date.now(),
  };

  const response = await request<FlexCheckDisbursementStatusResponsePayload>(ENDPOINT, payload);
  return response.data.map((d) => ({ refCode: d.ref_code, status: d.status }));
};

export default checkDisbursementStatus;
