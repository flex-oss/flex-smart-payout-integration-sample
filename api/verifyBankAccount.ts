import isEmpty from "lodash/isEmpty";
import { FlexBaseRequestPayload, FlexBaseResponsePayload } from "../types";
import { request } from "../utils";

type FlexBankAccount = {
  bank_id: string;
  bank_name: string;
  account_no: string;
  account_name: string;
};

type FlexVerifyBankAccountRequestPayload = {
  bank_accounts: {
    bank_id: string;
    account_no: string;
  }[];
} & FlexBaseRequestPayload;

type FlexVerifyBankAccountResponsePayload = {
  data: (FlexBankAccount & FlexBaseResponsePayload)[];
};

export enum VERIFY_BANK_ACCOUNT_RESPONSE_CODES {
  ACCOUNT_INFO_CORRECT = "ACCOUNT_INFO_CORRECT",
  ACCOUNT_NUMBER_INCORRECT = "ACCOUNT_NUMBER_INCORRECT",
}

type VerifyBankAccountResponse =
  | {
      isCorrect: false;
      code: VERIFY_BANK_ACCOUNT_RESPONSE_CODES;
      flexBankName?: string;
      flexBankCode?: string;
      accountName?: string;
    }
  | {
      isCorrect: true;
      code: VERIFY_BANK_ACCOUNT_RESPONSE_CODES;
      flexBankName: string;
      flexBankCode: string;
      accountName: string;
    };

const ENDPOINT = "partner/v1/bank/verify";

const verifyBankAccount = async (
  flexBankCode: string,
  flexBankName: string,
  accountNumber: string
): Promise<VerifyBankAccountResponse> => {
  if (
    flexBankCode === "" ||
    flexBankCode == null ||
    flexBankName === "" ||
    flexBankName == null
  ) {
    throw new Error("Bank is not supported");
  }

  const payload: FlexVerifyBankAccountRequestPayload = {
    request_time: Date.now(),
    bank_accounts: [
      {
        bank_id: flexBankCode,
        account_no: accountNumber,
      },
    ],
  };

  const response = await request<FlexVerifyBankAccountResponsePayload>(
    ENDPOINT,
    payload
  );

  if (isEmpty(response.data) || response.data[0]?.result_code !== "SUCCESS") {
    return {
      isCorrect: false,
      code: VERIFY_BANK_ACCOUNT_RESPONSE_CODES.ACCOUNT_NUMBER_INCORRECT,
    };
  }

  const [account] = response.data;

  return {
    isCorrect: true,
    code: VERIFY_BANK_ACCOUNT_RESPONSE_CODES.ACCOUNT_INFO_CORRECT,
    flexBankCode,
    flexBankName,
    accountName: account?.account_name,
  };
};

export default verifyBankAccount;
