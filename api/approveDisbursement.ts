import { FlexBaseRequestPayload, FlexBaseResponsePayload, FLEX_DISBURSEMENT_STATUSES } from "../types";
import { request } from "../utils";

type FlexApproveDisbursementRequestPayload = {
  data: {
    id: string;
    action: "approve";
    note: string;
  }[];
} & FlexBaseRequestPayload;

type FlexApproveDisbursementResponsePayload = {
  data: [
    {
      id: string;
      status: FLEX_DISBURSEMENT_STATUSES;
    }
  ];
} & FlexBaseResponsePayload;

const ENDPOINT = "partner/v1/disbursement/approval";

const approveDisbursement = async (disbursementId: string): Promise<void> => {
  const payload: FlexApproveDisbursementRequestPayload = {
    request_time: Date.now(),
    data: [
      {
        id: disbursementId,
        action: "approve",
        note: "Approve from API",
      },
    ],
  };

  const response = await request<FlexApproveDisbursementResponsePayload>(ENDPOINT, payload, undefined, {
    retry: { numberOfRetries: 3 },
  });

  if (response.result_code !== "SUCCESS") {
    throw new Error("Disbursement approval encounter error", { cause: response });
  }
};

export default approveDisbursement;
