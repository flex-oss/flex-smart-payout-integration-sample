import * as dotenv from "dotenv";
import {
  FlexBaseRequestPayload,
  FLEX_DISBURSEMENT_STATUSES,
  FlexRequestHeaders,
} from "../types";
import { generateSignature } from "../utils";

dotenv.config();
const { FLEX_PARTNER_ID } = process.env;

type DisbursementStatusChangedWebhookRequestPayload = {
  data: {
    id: string;
    ref_code: string;
    status: FLEX_DISBURSEMENT_STATUSES;
  } & FlexBaseRequestPayload;
};

const handleSuccessDisbursement = async (
  paymentProviderTransactionRefCode: string,
  payload: DisbursementStatusChangedWebhookRequestPayload,
  headers: FlexRequestHeaders
) => {
  console.info("----handleSuccessDisbursement------", {
    paymentProviderTransactionRefCode,
    payload,
    headers,
  });
};

const handleFailedDisbursement = async (
  paymentProviderTransactionRefCode: string,
  payload: DisbursementStatusChangedWebhookRequestPayload,
  headers: FlexRequestHeaders
) => {
  console.info("handleFailedDisbursement data", {
    paymentProviderTransactionRefCode,
    payload,
    headers,
  });
};

const disbursementStatusChanged = async (
  jsonPayload: string,
  headers: FlexRequestHeaders
) => {
  console.info(JSON.stringify({ headers, jsonPayload }, null, 2));
  const signature =
    headers["X-SIGNATURE"] ?? headers["X-Signature"] ?? headers["x-signature"];
  const partnerId =
    headers["X-PARTNER-ID"] ??
    headers["X-Partner-Id"] ??
    headers["x-partner-id"];
  if (partnerId !== FLEX_PARTNER_ID) {
    throw new Error("Invalid partner id");
  }
  const generatedSignature = generateSignature(jsonPayload);
  if (generatedSignature !== signature) {
    throw new Error("Invalid signature");
  }

  const payload = JSON.parse(
    jsonPayload
  ) as DisbursementStatusChangedWebhookRequestPayload;

  const {
    data: { id: paymentProviderTransactionRefCode, status },
  } = payload;

  switch (status) {
    case FLEX_DISBURSEMENT_STATUSES.DONE:
      await handleSuccessDisbursement(
        paymentProviderTransactionRefCode,
        payload,
        headers
      );
      return;
    case FLEX_DISBURSEMENT_STATUSES.FAILURE:
      await handleFailedDisbursement(
        paymentProviderTransactionRefCode,
        payload,
        headers
      );
      return;
    default:
      console.info("Unhandled disbursement status", status);
  }
};

export default disbursementStatusChanged;
