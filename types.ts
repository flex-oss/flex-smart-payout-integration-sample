type FlexResultCode = "SUCCESS" | "FAILURE";

export enum FLEX_TRANSACTION_LOG_TYPE {
  WEBHOOK_DISBURSEMENT_STATUS_CHANGED = "WEBHOOK_DISBURSEMENT_STATUS_CHANGED",
  REQUEST_CREATE_DISBURSEMENT = "REQUEST_CREATE_DISBURSEMENT",
}

export type FlexRequestHeaders =
  | {
      "Content-Type"?: string;
      "X-PARTNER-ID"?: string;
      "X-ACCESS-KEY"?: string;
      "X-SIGNATURE"?: string;
      "X-Signature"?: string;
      "X-Partner-Id"?: string;
      "X-Access-Key"?: string;
      "x-signature"?: string;
      "x-partner-id"?: string;
      "x-access-key"?: string;
    }
  | Record<string, string>;

export type FlexBaseRequestPayload = {
  request_time: number;
};

export type FlexBaseResponsePayload = {
  result_code?: FlexResultCode;
  result_message?: string;
};

export enum FLEX_DISBURSEMENT_STATUSES {
  NEW = "NEW",
  // Approved by User
  APPROVED = "APPROVED",
  // Rejected by Flex
  REJECT = "REJECT",
  // Payment is successful
  DONE = "DONE",
  FAILURE = "FAILURE",
}
