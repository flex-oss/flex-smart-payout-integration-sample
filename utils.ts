import { createHmac, createHash } from "crypto";
import fetch, { HeadersInit } from "node-fetch";
import { setTimeout } from "timers/promises";
import * as dotenv from "dotenv";

import { FlexRequestHeaders } from "./types";

dotenv.config();
const { FLEX_API_DOMAIN, FLEX_SECRET_KEY, FLEX_ACCESS_KEY, FLEX_PARTNER_ID } =
  process.env;

export const generateSignature = (jsonPayload: string) => {
  if (!FLEX_SECRET_KEY) {
    throw new Error("secret key is required");
  }

  const md5Payload = createHash("md5", { encoding: "utf8" })
    .update(jsonPayload)
    .digest("hex");
  const hmacPayload = createHmac("sha256", FLEX_SECRET_KEY, {
    encoding: "utf8",
  })
    .update(md5Payload)
    .digest("hex");

  return hmacPayload;
};

export const request = async <R>(
  endpoint: string,
  payload: Record<string, unknown>,
  headers: HeadersInit = {},
  options: { retry?: { numberOfRetries?: number; retryInterval?: number } } = {}
): Promise<R> => {
  if (!FLEX_SECRET_KEY || !FLEX_ACCESS_KEY || !FLEX_PARTNER_ID) {
    throw new Error(
      "Missing FLEX_SECRET_KEY, FLEX_ACCESS_KEY, FLEX_PARTNER_ID ENV"
    );
  }
  console.info(
    "----FLEX REQUEST----",
    JSON.stringify({ endpoint, payload }, null, 2)
  );

  // Have to JSON.stringify here because the signature require stable order of payload keys
  const jsonPayload = JSON.stringify(payload);
  const signature = generateSignature(jsonPayload);
  const requestHeaders: FlexRequestHeaders = {
    ...headers,
    "Content-Type": "application/json",
    "X-PARTNER-ID": FLEX_PARTNER_ID,
    "X-ACCESS-KEY": FLEX_ACCESS_KEY,
    "X-SIGNATURE": signature,
  };
  let result: R | undefined;

  let attempts = 0;
  const maxAttempts = options.retry?.numberOfRetries ?? 1;
  const retryInterval = options.retry?.retryInterval ?? 5000;

  while (attempts < maxAttempts) {
    try {
      const response = await fetch(`${FLEX_API_DOMAIN}/${endpoint}`, {
        headers: requestHeaders as HeadersInit,
        body: jsonPayload,
        method: "POST",
      });
      console.info(
        jsonPayload,
        JSON.stringify(
          {
            endpoint: `${FLEX_API_DOMAIN}/${endpoint}`,
            signature,
            requestHeaders,
          },
          null,
          2
        )
      );
      result = await (response.json() as Promise<R>);
      break;
    } catch (error) {
      attempts += 1;
      console.error(`Flex request failed: ${attempts} time(s)`, error);
      if (attempts < maxAttempts) {
        console.info(`Retrying in ${retryInterval} milliseconds...`);
        await setTimeout(retryInterval);
      }
    }
  }
  if (result == null) {
    throw new Error("Flex request failed");
  }
  console.info("----FLEX REQUEST----", JSON.stringify({ result }, null, 2));
  return result;
};
